package com.example.bluetoothsample;

import java.io.File;
import java.util.ArrayList;
import java.util.Set;
import android.support.v7.app.ActionBarActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;


public class BlueTooth extends ActionBarActivity implements OnClickListener 
{

	Button send,turn_on,visible,browse,turn_off;
	ListView show_list;
	public static String filepath; 
	//Android provides BluetoothAdapter class to communicate with Bluetooth
	public static BluetoothAdapter BA;
	private static int RESULT_LOAD_IMAGE = 1;
		
	private Set<BluetoothDevice>pairedDevices;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getSupportActionBar().hide();
		send=(Button)findViewById(R.id.btnSend);
		turn_on=(Button)findViewById(R.id.btnturnon);
		visible=(Button)findViewById(R.id.btnvisible);
		browse=(Button)findViewById(R.id.btnbrowse);
		turn_off=(Button)findViewById(R.id.btnturnoff);
		
		send.setOnClickListener(this);
		turn_on.setOnClickListener(this);
		turn_off.setOnClickListener(this);
		visible.setOnClickListener(this);
		browse.setOnClickListener(this);
		
		//Create object for Bluetooth Adapter Class
		BA = BluetoothAdapter.getDefaultAdapter();
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) 
	{
		if(v.getId()==R.id.btnSend)
		{
			
			pairedDevices = BA.getBondedDevices();
		      ArrayList list = new ArrayList();
		      
		      for(BluetoothDevice bt : pairedDevices)
		      {		      
		      Log.e("Name",bt.getName());
		      Log.e("Address",bt.getAddress());
		      Toast.makeText(getApplicationContext(),"Showing Paired Devices"+bt.getName(),Toast.LENGTH_SHORT).show();
		      }		      
		      
		      Toast.makeText(getApplicationContext(),"Showing Paired Devices",Toast.LENGTH_SHORT).show();
		     
			Intent intent = new Intent();
		    intent.setAction(Intent.ACTION_SEND);
		    intent.setType("image/*");
		    intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(filepath)));
		    startActivity(intent);			
			
		}
		if(v.getId()==R.id.btnturnoff)
		{
			BA.disable();
			Toast.makeText(getApplicationContext(),"Blue Tooth Turned off",Toast.LENGTH_LONG).show();
		}
		if(v.getId()==R.id.btnvisible)
		{
			Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			enableBtIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION,500);
			
            startActivityForResult(enableBtIntent,0);  
		}
		if(v.getId()==R.id.btnturnon)
		{
			if (!BA.isEnabled()) {
		         Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
		         startActivityForResult(turnOn, 0);
		         Toast.makeText(getApplicationContext(),"Turned on",Toast.LENGTH_LONG).show();
		      }
		      else
		      {
		         Toast.makeText(getApplicationContext(),"Already on", Toast.LENGTH_LONG).show();
		      }
		}
		
		
		if(v.getId()==R.id.btnbrowse)
		{
			Intent i = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
             
            startActivityForResult(i, RESULT_LOAD_IMAGE);
		}		
	}
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
         
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
 
            Cursor cursor = getContentResolver().query(selectedImage,filePathColumn, null, null, null);
            cursor.moveToFirst();
 
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            filepath=picturePath;
            cursor.close();
             
            ImageView imageView = (ImageView) findViewById(R.id.image);
            imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
         
        }
     
     
    }
	
	
	}
	